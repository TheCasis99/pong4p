﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {



	private Vector3 newPos;

	public float speed;
	private float move;

	void Start()
	{
		// Equivalente a playerTransform = GetComponent<Transform> ();


		newPos = transform.position;
	}

	void Update () 
	{
		
		//Read input
		move = Input.GetAxis("Vertical");

		move *= Time.deltaTime;
		move *= speed;

		transform.Translate(0, move, 0);

		/*
		if (transform.position.y < -4.4f) {
			transform.position = new Vector3 (transform.position.x, -4.4f, transform.position.z);
		}else if(transform.position.y > 4.4f) {
			transform.position = new Vector3 (transform.position.x, 4.4f, transform.position.z);
		}
		*/
	}
}
